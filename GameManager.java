public class GameManager {
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	public GameManager() {
		this.drawPile = new Deck();
		
		drawPile.shuffle();
		
		this.centerCard = drawPile.drawTopCard();
		this.playerCard = drawPile.drawTopCard();	
	}
	
	public String toString() {
		return this.centerCard + "\n" + this.playerCard;
	}
	
	public void dealCards() {
		drawPile.shuffle();
		this.centerCard = drawPile.drawTopCard();
		
		drawPile.shuffle();
		this.playerCard = drawPile.drawTopCard();
	}
	
	public int getNumberOfCards() {
		return drawPile.length();
	}
	
	public int calculatePoints() {
		if(centerCard.getValue() == playerCard.getValue()) {
			return 4;
		}
		if(centerCard.getSuit() == playerCard.getSuit()) {
			return 2;
		}
		return -1;
	}
	
}