import java.util.Scanner;

public class LuckyCardGameApp{
	public static void main(String[] args){
		
		System.out.println("Hello! Thanks for playing the Lucky! Card game :)");
		
		GameManager manager = new GameManager();
		int totalPoints = 0;
		int round = 1;
		
		while(manager.getNumberOfCards() > 1 && totalPoints < 5) {
				System.out.println("ROUND: " + round);
				round++;
				
				System.out.println("-----------------");
				System.out.println(manager);
				System.out.println("-----------------");
				
				totalPoints += manager.calculatePoints();
				System.out.println("Your points: " + totalPoints);
				System.out.println("");

				manager.dealCards();
		}
		if(totalPoints >= 5) {
			System.out.println("Player wins with " + totalPoints + " points!");
		}
		else {
			System.out.println("Player loses with " + totalPoints + " points :(");
		}
	}
}